#!/usr/bin/env bash

set -eux

readonly platform=$(nix-instantiate --eval --expr 'builtins.currentSystem' | tr -d "\"")
readonly kernel=$(echo $platform | cut -d- -f2)
readonly hostname=$(hostname)
readonly username=$(whoami)

nix build .#homeConfigs."${platform}"."$hostname"."${username}".activationPackage --show-trace
./result/activate

[[ $kernel != darwin ]] && exit 0

/run/current-system/sw/bin/darwin-rebuild switch --flake ./flakes/nix-darwin
