#!/usr/bin/env bash

set -eux

curl \
  --proto '=https' \
  --tlsv1.2 \
  -sSf \
  -L https://install.determinate.systems/nix \
  --output /tmp/nix-installer-downloader
  
sh /tmp/nix-installer-downloader install

set +eux
. /nix/var/nix/profiles/default/etc/profile.d/nix-daemon.sh
set -eux

os=$(nix-instantiate --eval --expr builtins.currentSystem | tr -d "\"" | cut -d- -f2)
[[ ${os} != darwin ]] && exit 0

[[ ! -L /etc/nix/nix.conf && -f /etc/nix/nix.conf ]] && \
  sudo mv /etc/nix/nix.conf /etc/nix/nix.before-nix-darwin

nix run nix-darwin \
  --extra-experimental-features nix-command \
  --extra-experimental-features flakes \
  -- \
    switch \
      --flake ../nix/flakes/nix-darwin
