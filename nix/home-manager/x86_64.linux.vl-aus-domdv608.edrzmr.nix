{ config, pkgs, ... }: {

  home.username = "edrzmr";
  home.homeDirectory = "/home/edrzmr";
  home.stateVersion = "24.05";

  home.sessionVariables = {
  };

  home.sessionPath = [
    "$HOME/.local/bin"
  ];

  home.packages = with pkgs; [
    kubectl
    kubernetes-helm
    unrar

    #coursier
  ];

  programs.vim = import ./modules/vim.nix pkgs;
  programs.zsh = import ./modules/zsh.nix pkgs;
  programs.direnv = import ./modules/direnv.nix pkgs;
  programs.password-store = import ./modules/pass.nix pkgs;
  programs.starship = import ./modules/starship.nix pkgs;

  # envrc.d
  #home.file.".config/envrc.d/dasera-discovery.envrc".source = ./files/envrc.d/dasera-discovery.envrc;
  #home.file.".config/envrc.d/dasera-ui.envrc".source = ./files/envrc.d/dasera-ui.envrc;

  home.file.".config/user-dirs.dirs".source = ./files/config/user-dirs.dirs;
  home.file.".config/user-dirs.conf".source = ./files/config/user-dirs.conf;
  #home.file.".local/share/applications/firefox-devedition.desktop".source = ./files/local/share/applications/firefox-devedition.desktop;
  home.file.".zsh/completions/.placeholder".text = "";
}
