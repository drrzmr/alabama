pkgs: {
  enable = true;
  plugins = with pkgs.vimPlugins; [
    molokai
    vim-nix
    vim-airline
    editorconfig-vim
    vim-gnupg
  ];
  settings = {
    modeline = true;
    number = true;
    relativenumber = false;
    background = "dark";
    ignorecase = false;
    shiftwidth = 2;
    tabstop = 2;
    expandtab = true;
  };
  extraConfig = ''
    colorscheme molokai

    filetype plugin indent on

    " enable mouse click
    set mouse=a

    " allow backspacing over everything in insert mode
    set backspace=indent,eol,start

    " configure chars used by set list
    set listchars=tab:\|+,trail:.,extends:>,precedes:<

    " .c, .h
    au BufRead,BufNewFile *.c,*.h set tabstop=4
    au BufRead,BufNewFile *.c,*.h set shiftwidth=4
    au BufRead,BufNewFile *.c,*.h set expandtab!
    au BufRead,BufNewFile *.c,*.h highlight OverLength ctermbg=red ctermfg=white guibg=red guifg=whte
    au BufRead,BufNewFile *.c,*.h match OverLength '\%81v.\+'

    " .py
    au BufRead,BufNewFile *.py set tabstop=4
    au BufRead,BufNewFile *.py set shiftwidth=4

    " .avsc (avro schema)
    au BufRead,BufNewFile *.avsc set filetype=javascript
  '';
}
