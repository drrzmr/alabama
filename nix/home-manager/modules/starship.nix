pkgs: {
  enable = true;
  settings = {
    add_newline = false;
    format = "$directory$character";
    right_format = "$all";
    command_timeout = 500;

    directory = {
      disabled = false;
      truncate_to_repo = false;
      style = "inverted bold bright-blue";
    };

    scala = {
      disabled = true;
    };

    java = {
      disabled = true;
    };

    memory_usage = {
      disabled = true;
      threshold = -1;
    };

    aws = {
      disabled = true;
    };

  };
}
