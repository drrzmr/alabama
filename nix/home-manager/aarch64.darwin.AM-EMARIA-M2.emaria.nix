{ config, pkgs, ... }: {

  home.username = "emaria";
  home.homeDirectory = "/Users/emaria";
  home.stateVersion = "24.05";

  home.sessionVariables = {
    _LOCAL_BREW_DIR = "/opt/homebrew";
  };

  home.sessionPath = [
    "$HOME/.local/bin"
  ];

  home.packages = with pkgs; [
    btop
    htop
    tig
    tree
    pstree
    neofetch
    jq
    qpdf
    m-cli
    yq
    gnused
    unixtools.netstat

    kubectl
    kubernetes-helm
  ];

  home.file."Library/KeyBindings/DefaultKeyBinding.dict".source = ./files/darwin-keybinding/DefaultKeyBinding.dict;
  programs.vim = import ./modules/vim.nix pkgs;
  programs.zsh = import ./modules/zsh.nix pkgs;
  programs.direnv = import ./modules/direnv.nix pkgs;
  programs.password-store = import ./modules/pass.nix pkgs;
  programs.starship = import ./modules/starship.nix pkgs;

  home.file.".zsh/completions/.placeholder".text = "";
}
