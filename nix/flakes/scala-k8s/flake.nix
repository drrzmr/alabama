{
  description = "Scala Dev && k8s";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
      in
      {
        devShells.default = pkgs.mkShell {
          
          packages = [
            pkgs.k3d
            pkgs.kubectl
            pkgs.kubernetes-helm
          ];

          shellHook = ''
            echo lepo
          '';

        };
      }

    );
}

