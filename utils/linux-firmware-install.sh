#!/bin/bash

set -eu

[[ $(id -u) -ne 0 ]] && echo "must be root" && exit 0

echo && env | grep ^FIRM && echo
# clean
find /lib/firmware -xtype l -delete       # delete broken links
find /lib/firmware -type d -empty -delete # delete empty folders

targetDir="${FIRM_TARGET_BASE_DIR:-/opt/linux-firmware}" && mkdir -p "${targetDir}"

declare -A repos
repos[main]="git://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git"
repos[asus]="https://gitlab.com/asus-linux/firmware.git"
repos[nvidia]="https://github.com/NVIDIA/linux-firmware.git"

declare -A names
names[main]="linux-firmware-main"
names[asus]="linux-firmware-asus"
names[nvidia]="linux-firmware-nvidia"

echo -e "\n=== firmware repos ==="
for key in "${!repos[@]}"; do echo "=> ${names[$key]} > ${repos[$key]}"; done
echo "===================="

gitCommands=''
for key in "${!repos[@]}"; do
  target="${targetDir}/${names[$key]}"
  repo="${repos[$key]}"
  [[ ! -d $target ]] && gitCommands="git clone ${repo} ${target}\n${gitCommands}"
  [[ -d $target ]] && gitCommands="cd ${target} && git pull && cd -\n${gitCommands}"
done

echo -e "\n=== git commands ==="
echo -ne "${gitCommands}"
echo -e "====================\n"

# execute git commands
echo -ne "${gitCommands}" | xargs -P ${#repos[@]} -I {} sh -c "eval {}" # perfom parallel repos clone/pull

OLD_IFS=${IFS}
IFS=
for key in "${!repos[@]}"; do                           # all repos
  target="${targetDir}/${names[$key]}"
  repo="${repos[$key]}"

  cd "${target}"
  readarray -d '' firmwares < <(find . -type f -print0) # load all firmwares from current repo
  for i in "${firmwares[@]}"; do                        # for each firmware (repo file)
    firmware=$(cut -d"/" -f2- <<< "$i")
    [[ $firmware =~ ^.git ]] && continue                # filter git directory

    cd /lib/firmware
    [[ -f $firmware || -h $firmware || -d $firmware ]] && continue # filter existent firmware

    # handle link creation
    dir=$(dirname "/lib/firmware/$firmware")
    mkdir -pv "${dir}"

    link="${target}/${firmware}"
    cd "$dir"
    echo "=> path: ${PWD}, link: ${link}"
    ln -s "${link}" .

  done
done
IFS=${OLD_IFS}
