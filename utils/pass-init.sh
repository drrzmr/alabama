#!/usr/bin/env bash

set -x

pass init eder.rzmr@proton.me
pass git init --initial-branch pass
pass git remote add origin git@gitlab.com:edrzmr/dotfiles.git
pass git fetch origin --prune
pass git branch --set-upstream-to=origin/pass pass
pass git reset --hard origin/pass
